package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 操作记录
 */
@Data
public class ActionRecord implements Serializable {
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * 主键
     */
    private Long id;

    /**
     * 药品ID
     */
    private Long drugId;

    /**
     * 药品名称
     */
    private String drugName;

    /**
     * 操作
     */
    private String action;

    /**
     * 操作时间
     */
    private Date actionTime;

    /**
     * 操作数量
     */
    private Integer amount;

    private static final long serialVersionUID = 1L;
}