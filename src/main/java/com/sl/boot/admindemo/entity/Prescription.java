package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 药方
 */
@Data
public class Prescription implements Serializable {
    private Integer id;

    /**
     * 药方说明
     */
    private String description;

    /**
     * 用法
     */
    private String usage;

    /**
     * 药方状态
     */
    private String status;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 属于哪个患者
     */
    private String belongToPatientName;

    /**
     * 开局药方的医生的姓名
     */
    private String belongToDoctorName;

    /**
     * 医生ID
     */
    private Integer did;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getBelongToPatientName() {
        return belongToPatientName;
    }

    public void setBelongToPatientName(String belongToPatientName) {
        this.belongToPatientName = belongToPatientName;
    }

    public String getBelongToDoctorName() {
        return belongToDoctorName;
    }

    public void setBelongToDoctorName(String belongToDoctorName) {
        this.belongToDoctorName = belongToDoctorName;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    private static final long serialVersionUID = 1L;
}