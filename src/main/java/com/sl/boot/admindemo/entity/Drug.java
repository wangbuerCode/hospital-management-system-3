package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 药品
 */
@Data
public class Drug implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 药品名称
     */
    private String drugName;

    /**
     * 药品数量
     */
    private Long drugCount;

    /**
     * 生产厂家
     */
    private String factory;

    /**
     * 价格
     */
    private Long price;

    /**
     * 生产日期
     */
    private Date productionTime;

    /**
     * 药品功能
     */
    private String drugFunction;

    /**
     * 药品规格
     */
    private String size;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Long getDrugCount() {
        return drugCount;
    }

    public void setDrugCount(Long drugCount) {
        this.drugCount = drugCount;
    }

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Date getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(Date productionTime) {
        this.productionTime = productionTime;
    }

    public String getDrugFunction() {
        return drugFunction;
    }

    public void setDrugFunction(String drugFunction) {
        this.drugFunction = drugFunction;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    private static final long serialVersionUID = 1L;
}