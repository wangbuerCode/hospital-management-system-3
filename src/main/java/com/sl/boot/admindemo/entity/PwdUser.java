package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author
 */
@Data
public class PwdUser implements Serializable {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 类型 1-管理员 2-药库管理员 3-医生 4-病患
     */
    private Integer userType;

    private static final long serialVersionUID = 1L;
}