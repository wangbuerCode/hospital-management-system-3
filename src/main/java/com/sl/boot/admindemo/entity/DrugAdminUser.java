package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 药库管理员
 */
@Data
public class DrugAdminUser implements Serializable {
    private Integer id;

    /**
     * 账号
     */
    private String drugAdminUsername;

    /**
     * 添加时间
     */
    private Date inserttime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDrugAdminUsername() {
        return drugAdminUsername;
    }

    public void setDrugAdminUsername(String drugAdminUsername) {
        this.drugAdminUsername = drugAdminUsername;
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    private static final long serialVersionUID = 1L;
}