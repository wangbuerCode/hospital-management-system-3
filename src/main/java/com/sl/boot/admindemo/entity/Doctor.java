package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 医生
 */
@Data
public class Doctor implements Serializable {
    private Integer id;

    private String doctorname;

    private Date inserttime;

    /**
     * 医生姓名
     */
    private String anoName;

    /**
     * 身份证号码
     */
    private String idCard;

    /**
     * 手机号码
     */
    private String phoneNum;

    /**
     * 科室
     */
    private String section;

    /**
     * 职称
     */
    private String tTitle;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public String getAnoName() {
        return anoName;
    }

    public void setAnoName(String anoName) {
        this.anoName = anoName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String gettTitle() {
        return tTitle;
    }

    public void settTitle(String tTitle) {
        this.tTitle = tTitle;
    }

    private static final long serialVersionUID = 1L;
}