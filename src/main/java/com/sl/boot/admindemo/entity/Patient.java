package com.sl.boot.admindemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
@Data
public class Patient implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 账号
     */
    private String patientName;

    /**
     * 添加时间
     */
    private Date addTime;

    /**
     * 密码
     */
    private String password;

    /**
     * 患病
     */
    private String disease;

    /**
     * 年龄
     */
    private String age;

    /**
     * 性别
     */
    private String gender;

    /**
     * 药方ID
     */
    private Integer method;

    /**
     * 属于哪个医生
     */
    private Long belongToDoctorId;

    /**
     * 患者真实姓名
     */
    private String anoName;

    /**
     * 身份证号码
     */
    private String idCard;

    /**
     * 医疗卡号
     */
    private String medicalId;

    /**
     * 住院或门诊
     */
    private String ptype;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public Long getBelongToDoctorId() {
        return belongToDoctorId;
    }

    public void setBelongToDoctorId(Long belongToDoctorId) {
        this.belongToDoctorId = belongToDoctorId;
    }

    public String getAnoName() {
        return anoName;
    }

    public void setAnoName(String anoName) {
        this.anoName = anoName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMedicalId() {
        return medicalId;
    }

    public void setMedicalId(String medicalId) {
        this.medicalId = medicalId;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    private static final long serialVersionUID = 1L;
}